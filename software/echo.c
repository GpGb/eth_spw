/*
 * Copyright (C) 2009 - 2019 Xilinx, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>

#include "lwip/err.h"
#include "lwip/tcp.h"
#if defined (__arm__) || defined (__aarch64__)
#include "xil_printf.h"
#endif

#include "xil_io.h"
#include "xaxidma.h"
#include "xparameters.h"

// Axi DMA instance
XAxiDma AxiDma;
#define DMA_DEV_ID		XPAR_AXIDMA_0_DEVICE_ID
#define MAX_ALP_PKT_LEN 4000

// last pcb
struct tcp_pcb *last_pcb = NULL;



int AxiDMA_init()
{
	XAxiDma_Config *CfgPtr;
	int Status = XST_SUCCESS;

	CfgPtr = XAxiDma_LookupConfig(DMA_DEV_ID);
	if (!CfgPtr) {
		return XST_FAILURE;
	}

	Status = XAxiDma_CfgInitialize(&AxiDma, CfgPtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	if(XAxiDma_HasSg(&AxiDma)){
		xil_printf("Device configured as SG mode \r\n");
		return XST_FAILURE;
	}

	// disable interrupts
	XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
						XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
						XAXIDMA_DMA_TO_DEVICE);

	Status = XAxiDma_Selftest(&AxiDma);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	return Status;
}


//TODO: interrupt-based
int transfer_data()
{
	static int dma_waiting = 0;
	static volatile uint8_t dma_rx_buffer[MAX_ALP_PKT_LEN];

	if (last_pcb == NULL)
		return ERR_OK;

	// receive any data from spacewire
	if (!dma_waiting){
		int status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) dma_rx_buffer,
						MAX_ALP_PKT_LEN, XAXIDMA_DEVICE_TO_DMA);

		if (status != XST_SUCCESS) {
			xil_printf("Dma simple xfer failed..\n\r");
			return ERR_OK;
		}
	}

	// wait rx completed
	if (XAxiDma_Busy(&AxiDma, XAXIDMA_DEVICE_TO_DMA)){
		dma_waiting = 1;
		return ERR_OK;
	}

	dma_waiting = 0;

	// read from DMA how much data was actually read
	uint32_t pkt_len = Xil_In32(XPAR_AXI_DMA_0_BASEADDR + 0x58);
	Xil_DCacheInvalidateRange((UINTPTR)dma_rx_buffer, pkt_len);

	// send tcp response with read data
	err_t err = ERR_OK;

	if (tcp_sndbuf(last_pcb) > pkt_len) {
		err = tcp_write(last_pcb, dma_rx_buffer, pkt_len, 1);
	}
	else {
		xil_printf("no space in tcp_sndbuf\n\r");
		err = ERR_BUF;
	}

	return err;
}


err_t recv_callback(void *arg, struct tcp_pcb *tpcb,
		struct pbuf *p, err_t err)
{
	// remote host closed connection
	if (!p) {
		tcp_close(tpcb);
		tcp_recv(tpcb, NULL);
		last_pcb = NULL;
		return ERR_OK;
	}

	/* indicate that the packet has been received */
	tcp_recved(tpcb, p->len);

	// send payload to spacewire
	int status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) p->payload,
					p->len, XAXIDMA_DMA_TO_DEVICE);

	if (status != XST_SUCCESS) {
		xil_printf("DMA send failure\r\n");
		pbuf_free(p);
		return ERR_OK;
	}

	// wait tx completed
	while (XAxiDma_Busy(&AxiDma, XAXIDMA_DMA_TO_DEVICE));

	// free the received pbuf
	pbuf_free(p);

	return ERR_OK;
}


err_t accept_callback(void *arg, struct tcp_pcb *newpcb, err_t err)
{

	tcp_arg(newpcb, NULL);

	/* set the receive callback for this connection */
	tcp_recv(newpcb, recv_callback);

	last_pcb = newpcb;

	return ERR_OK;
}


int start_application()
{
	struct tcp_pcb *pcb;
	err_t err;
	unsigned port = 7;

	/* create new TCP PCB structure */
	pcb = tcp_new_ip_type(IPADDR_TYPE_ANY);
	if (!pcb) {
		xil_printf("Error creating PCB. Out of Memory\n\r");
		return -1;
	}

	/* bind to specified @port */
	err = tcp_bind(pcb, IP_ANY_TYPE, port);
	if (err != ERR_OK) {
		xil_printf("Unable to bind to port %d: err = %d\n\r", port, err);
		return -2;
	}

	/* we do not need any arguments to callback functions */
	tcp_arg(pcb, NULL);

	/* listen for connections */
	pcb = tcp_listen(pcb);
	if (!pcb) {
		xil_printf("Out of memory while tcp_listen\n\r");
		return -3;
	}

	/* specify callback to use for incoming connections */
	tcp_accept(pcb, accept_callback);

	xil_printf("TCP echo server started @ port %d\n\r", port);


	int status = AxiDMA_init();

	if (status != XST_SUCCESS) {
		xil_printf("AxiDMA_init Failed\r\n");
		return -4;
	}

	return 0;
}
