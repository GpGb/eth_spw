#!/usr/bin/env tclsh

set out_dir "bin_out"
exec mkdir -p ${out_dir}

# load FPGA bitstream with bootloader.elf
exec updatemem \
	-meminfo ./eth_spwr/eth_spwr.runs/impl_1/eth2spw_top.mmi \
	-data eth_spwr/eth_spwr.vitis/bootloader/Debug/bootloader.elf \
	-proc mcu_i/microblaze_0 \
	-bit ./eth_spwr/eth_spwr.runs/impl_1/eth2spw_top.bit \
	-out ${out_dir}/eth_spwr.bit \
	-force

# copy MB application (eth server) and generate .srec
exec cp eth_spwr/eth_spwr.vitis/eth_server/Debug/eth_server.elf ${out_dir}
exec mb-objcopy -O srec ${out_dir}/eth_server.elf ${out_dir}/eth_server.srec

# generete .bin configuration file for the flash memory
write_cfgmem  -format bin -size 16 -interface SPIx1 \
	-loadbit  "up 0x00000000 ${out_dir}/eth_spwr.bit" \
	-loaddata "up 0x00300000 ${out_dir}/eth_server.srec" \
	-force \
	-file "${out_dir}/eth_spw.bin"
