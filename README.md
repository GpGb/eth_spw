Simple point-to-point ethrnet TCP <--> Spacewire adapter.
A tcp server running in the soft-cpu in the fpga listen for connection on the ethrnet interface and relays any tcp payload to/from the spacewire interface.
