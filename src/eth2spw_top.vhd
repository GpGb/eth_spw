library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.spwpkg.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity eth2spw_top is
	port (
		ddr3_sdram_addr : out std_logic_vector ( 13 downto 0 );
		ddr3_sdram_ba : out std_logic_vector ( 2 downto 0 );
		ddr3_sdram_cas_n : out std_logic;
		ddr3_sdram_ck_n : out std_logic_vector ( 0 to 0 );
		ddr3_sdram_ck_p : out std_logic_vector ( 0 to 0 );
		ddr3_sdram_cke : out std_logic_vector ( 0 to 0 );
		ddr3_sdram_cs_n : out std_logic_vector ( 0 to 0 );
		ddr3_sdram_dm : out std_logic_vector ( 1 downto 0 );
		ddr3_sdram_dq : inout std_logic_vector ( 15 downto 0 );
		ddr3_sdram_dqs_n : inout std_logic_vector ( 1 downto 0 );
		ddr3_sdram_dqs_p : inout std_logic_vector ( 1 downto 0 );
		ddr3_sdram_odt : out std_logic_vector ( 0 to 0 );
		ddr3_sdram_ras_n : out std_logic;
		ddr3_sdram_reset_n : out std_logic;
		ddr3_sdram_we_n : out std_logic;
		eth_mdio_mdc_mdc : out std_logic;
		eth_mdio_mdc_mdio_io : inout std_logic;
		eth_mii_col : in std_logic;
		eth_mii_crs : in std_logic;
		eth_mii_rst_n : out std_logic;
		eth_mii_rx_clk : in std_logic;
		eth_mii_rx_dv : in std_logic;
		eth_mii_rx_er : in std_logic;
		eth_mii_rxd : in std_logic_vector ( 3 downto 0 );
		eth_mii_tx_clk : in std_logic;
		eth_mii_tx_en : out std_logic;
		eth_mii_txd : out std_logic_vector ( 3 downto 0 );
		eth_ref_clk : out std_logic;
		reset : in std_logic;
		sys_clock : in std_logic;
		usb_uart_rxd : in std_logic;
		usb_uart_txd : out std_logic;
		qspi_dq  : inout std_logic_vector(3 downto 0);
		qspi_cs  : inout std_logic;
		qspi_sck : inout std_logic;
		-- spacwire	pins
		spw_di:     in  std_logic;
		spw_si:     in  std_logic;
		spw_do:     out std_logic;
		spw_so:     out std_logic
	);
end eth2spw_top;

architecture structure of eth2spw_top is

	component mcu is
		port (
			sys_clock : in std_logic;
			reset : in std_logic;
			usb_uart_rxd : in std_logic;
			usb_uart_txd : out std_logic;
			ddr3_sdram_dq : inout std_logic_vector ( 15 downto 0 );
			ddr3_sdram_dqs_p : inout std_logic_vector ( 1 downto 0 );
			ddr3_sdram_dqs_n : inout std_logic_vector ( 1 downto 0 );
			ddr3_sdram_addr : out std_logic_vector ( 13 downto 0 );
			ddr3_sdram_ba : out std_logic_vector ( 2 downto 0 );
			ddr3_sdram_ras_n : out std_logic;
			ddr3_sdram_cas_n : out std_logic;
			ddr3_sdram_we_n : out std_logic;
			ddr3_sdram_reset_n : out std_logic;
			ddr3_sdram_ck_p : out std_logic_vector ( 0 to 0 );
			ddr3_sdram_ck_n : out std_logic_vector ( 0 to 0 );
			ddr3_sdram_cke : out std_logic_vector ( 0 to 0 );
			ddr3_sdram_cs_n : out std_logic_vector ( 0 to 0 );
			ddr3_sdram_dm : out std_logic_vector ( 1 downto 0 );
			ddr3_sdram_odt : out std_logic_vector ( 0 to 0 );
			eth_mdio_mdc_mdc : out std_logic;
			eth_mdio_mdc_mdio_i : in std_logic;
			eth_mdio_mdc_mdio_o : out std_logic;
			eth_mdio_mdc_mdio_t : out std_logic;
			eth_mii_col : in std_logic;
			eth_mii_crs : in std_logic;
			eth_mii_rst_n : out std_logic;
			eth_mii_rx_clk : in std_logic;
			eth_mii_rx_dv : in std_logic;
			eth_mii_rx_er : in std_logic;
			eth_mii_rxd : in std_logic_vector ( 3 downto 0 );
			eth_mii_tx_clk : in std_logic;
			eth_mii_tx_en : out std_logic;
			eth_mii_txd : out std_logic_vector ( 3 downto 0 );
			eth_ref_clk : out std_logic;
			qspi_flash_io0_i : in std_logic;
			qspi_flash_io0_o : out std_logic;
			qspi_flash_io0_t : out std_logic;
			qspi_flash_io1_i : in std_logic;
			qspi_flash_io1_o : out std_logic;
			qspi_flash_io1_t : out std_logic;
			qspi_flash_io2_i : in std_logic;
			qspi_flash_io2_o : out std_logic;
			qspi_flash_io2_t : out std_logic;
			qspi_flash_io3_i : in std_logic;
			qspi_flash_io3_o : out std_logic;
			qspi_flash_io3_t : out std_logic;
			qspi_flash_sck_i : in std_logic;
			qspi_flash_sck_o : out std_logic;
			qspi_flash_sck_t : out std_logic;
			qspi_flash_ss_i  : in std_logic_vector(0 downto 0);
			qspi_flash_ss_o  : out std_logic_vector(0 downto 0);
			qspi_flash_ss_t  : out std_logic;
			clk_spw : out std_logic;
			M_AXIS_0_tvalid : out std_logic;
			M_AXIS_0_tready : in std_logic;
			M_AXIS_0_tdata : out std_logic_vector ( 7 downto 0 );
			S_AXIS_0_tvalid : in std_logic;
			S_AXIS_0_tready : out std_logic;
			S_AXIS_0_tdata : in std_logic_vector ( 7 downto 0 );
			M_AXIS_0_tlast : out std_logic;
			S_AXIS_0_tlast : in std_logic
		);
	end component mcu;

	signal eth_mdio_mdc_mdio_i : std_logic;
	signal eth_mdio_mdc_mdio_o : std_logic;
	signal eth_mdio_mdc_mdio_t : std_logic;

	signal M_AXIS_0_tdata, S_AXIS_0_tdata : std_logic_vector(7 downto 0);
	signal M_AXIS_0_tlast, M_AXIS_0_tready, M_AXIS_0_tvalid : std_logic;
	signal S_AXIS_0_tlast, S_AXIS_0_tready, S_AXIS_0_tvalid : std_logic;

	signal clk_spw, spw_rxclk, spw_txclk : std_logic := '0';
	signal rst : std_logic;

	signal spw_state_started, spw_state_connecting,
		spw_state_running : std_logic;
	signal spw_errdisc, spw_errpar, spw_erresc, spw_errcred : std_logic;

	signal qspi_dq_i, qspi_dq_o, qspi_dq_t : std_logic_vector(3 downto 0);
	signal qspi_cs_i, qspi_cs_o, qspi_cs_t : std_logic;
	signal qspi_sck_i, qspi_sck_o, qspi_sck_t : std_logic;

	attribute mark_debug : string;
	attribute mark_debug of spw_state_started, spw_state_connecting, spw_state_running : signal is "true";
	attribute mark_debug of spw_errdisc, spw_errpar, spw_erresc, spw_errcred : signal is "true";
	attribute mark_debug of spw_di, spw_si, spw_do, spw_so : signal is "true";

begin

	mcu_i: mcu
	port map (
		ddr3_sdram_addr(13 downto 0) => ddr3_sdram_addr(13 downto 0),
		ddr3_sdram_ba(2 downto 0) => ddr3_sdram_ba(2 downto 0),
		ddr3_sdram_cas_n => ddr3_sdram_cas_n,
		ddr3_sdram_ck_n(0) => ddr3_sdram_ck_n(0),
		ddr3_sdram_ck_p(0) => ddr3_sdram_ck_p(0),
		ddr3_sdram_cke(0) => ddr3_sdram_cke(0),
		ddr3_sdram_cs_n(0) => ddr3_sdram_cs_n(0),
		ddr3_sdram_dm(1 downto 0) => ddr3_sdram_dm(1 downto 0),
		ddr3_sdram_dq(15 downto 0) => ddr3_sdram_dq(15 downto 0),
		ddr3_sdram_dqs_n(1 downto 0) => ddr3_sdram_dqs_n(1 downto 0),
		ddr3_sdram_dqs_p(1 downto 0) => ddr3_sdram_dqs_p(1 downto 0),
		ddr3_sdram_odt(0) => ddr3_sdram_odt(0),
		ddr3_sdram_ras_n => ddr3_sdram_ras_n,
		ddr3_sdram_reset_n => ddr3_sdram_reset_n,
		ddr3_sdram_we_n => ddr3_sdram_we_n,
		eth_mdio_mdc_mdc => eth_mdio_mdc_mdc,
		eth_mdio_mdc_mdio_i => eth_mdio_mdc_mdio_i,
		eth_mdio_mdc_mdio_o => eth_mdio_mdc_mdio_o,
		eth_mdio_mdc_mdio_t => eth_mdio_mdc_mdio_t,
		eth_mii_col => eth_mii_col,
		eth_mii_crs => eth_mii_crs,
		eth_mii_rst_n => eth_mii_rst_n,
		eth_mii_rx_clk => eth_mii_rx_clk,
		eth_mii_rx_dv => eth_mii_rx_dv,
		eth_mii_rx_er => eth_mii_rx_er,
		eth_mii_rxd(3 downto 0) => eth_mii_rxd(3 downto 0),
		eth_mii_tx_clk => eth_mii_tx_clk,
		eth_mii_tx_en => eth_mii_tx_en,
		eth_mii_txd(3 downto 0) => eth_mii_txd(3 downto 0),
		eth_ref_clk => eth_ref_clk,
		reset => reset,
		sys_clock => sys_clock,
		usb_uart_rxd => usb_uart_rxd,
		usb_uart_txd => usb_uart_txd,
		qspi_flash_io0_i => qspi_dq_i(0),
		qspi_flash_io0_o => qspi_dq_o(0),
		qspi_flash_io0_t => qspi_dq_t(0),
		qspi_flash_io1_i => qspi_dq_i(1),
		qspi_flash_io1_o => qspi_dq_o(1),
		qspi_flash_io1_t => qspi_dq_t(1),
		qspi_flash_io2_i => qspi_dq_i(2),
		qspi_flash_io2_o => qspi_dq_o(2),
		qspi_flash_io2_t => qspi_dq_t(2),
		qspi_flash_io3_i => qspi_dq_i(3),
		qspi_flash_io3_o => qspi_dq_o(3),
		qspi_flash_io3_t => qspi_dq_t(3),
		qspi_flash_ss_i(0) => qspi_cs_i,
		qspi_flash_ss_o(0) => qspi_cs_o,
		qspi_flash_ss_t => qspi_cs_t,
		qspi_flash_sck_i => qspi_sck_i,
		qspi_flash_sck_o => qspi_sck_o,
		qspi_flash_sck_t => qspi_sck_t,
		clk_spw => clk_spw,
		M_AXIS_0_tdata  => M_AXIS_0_tdata,
		M_AXIS_0_tlast  => M_AXIS_0_tlast,
		M_AXIS_0_tready => M_AXIS_0_tready,
		M_AXIS_0_tvalid => M_AXIS_0_tvalid,
		S_AXIS_0_tdata  => S_AXIS_0_tdata,
		S_AXIS_0_tlast  => S_AXIS_0_tlast,
		S_AXIS_0_tready => S_AXIS_0_tready,
		S_AXIS_0_tvalid => S_AXIS_0_tvalid
	);


	spacewire_impl: entity work.spwstream
	generic map (
		sysfreq   => 100.0e6,
		txclkfreq => 100.0e6,
		rximpl    => impl_generic,
		rxchunk   => 1,
		tximpl    => impl_generic,
		rxfifosize_bits => 11,
		txfifosize_bits => 11
	)
	port map (
		clk        => clk_spw,
		rxclk      => spw_rxclk,
		txclk      => spw_txclk,
		rst        => rst,
		autostart  => '1',
		linkstart  => '1',
		linkdis    => '0',
		txdivcnt   => x"01",
		tick_in    => '0',
		ctrl_in    => "11",
		time_in    => "111111",
		txwrite    => M_AXIS_0_tvalid,
		txflag     => M_AXIS_0_tlast,
		txdata     => M_AXIS_0_tdata,
		txrdy      => M_AXIS_0_tready,
		txhalff    => open,
		tick_out   => open,
		ctrl_out   => open,
		time_out   => open,
		rxvalid    => S_AXIS_0_tvalid,
		rxhalff    => open,
		rxflag     => S_AXIS_0_tlast,
		rxdata     => S_AXIS_0_tdata,
		rxread     => S_AXIS_0_tready,
		started    => spw_state_started,
		connecting => spw_state_connecting,
		running    => spw_state_running,
		errdisc    => spw_errdisc,
		errpar     => spw_errpar,
		erresc     => spw_erresc,
		errcred    => spw_errcred,
		spw_di     => spw_di,
		spw_si     => spw_si,
		spw_do     => spw_do,
		spw_so     => spw_so
	);


	eth_mdio_mdc_mdio_iobuf: IOBUF
		port map (
			I => eth_mdio_mdc_mdio_o,
			IO => eth_mdio_mdc_mdio_io,
			O => eth_mdio_mdc_mdio_i,
			T => eth_mdio_mdc_mdio_t
		);


	qspi_iobuf: for n in 0 to 3 generate
		qspi_dq_IOBUF_inst : IOBUF
			port map(
				O  => qspi_dq_i(n),
				IO => qspi_dq(n),
				I  => qspi_dq_o(n),
				T  => qspi_dq_t(n)
			);
	end generate;

	qspi_ss_IOBUF_inst : IOBUF
		port map(
			O  => qspi_cs_i,
			IO => qspi_cs,
			I  => qspi_cs_o,
			T  => qspi_cs_t
		);

	qspi_sck_IOBUF_inst : IOBUF
		port map(
			O  => qspi_sck_i,
			IO => qspi_sck,
			I  => qspi_sck_o,
			T  => qspi_sck_t
		);

end structure;
